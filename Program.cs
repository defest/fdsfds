﻿using System.Text;

internal class Program
{
    public static object locker = new object();
    static void DoWork(int max)
    {
        lock (locker)
        {
            for (int i = 0; i < max; i++)
            {
                Console.WriteLine("DoWork");
            }
        }
    }
    static void DoWork2(object max)
    {
        int j = 0;
        for (int i = 0; i < (int)max; i++)
        {
            j++;
            if (j % 10000 == 0)
                Console.WriteLine("DoWork 2");
        }
    }
    static async Task DoWorkAsync(int max)
    {
        await Task.Run(()=>DoWork( max));
        Console.WriteLine("DoWorkAsync");
    }
    static bool SaveFile(string p)
    {
        Random random = new Random();
        string text = "";
        for (int i = 0; i < 50000; i++)
        {
            text += random.Next();
        }
        using (StreamWriter streamWriter =new StreamWriter(p, false,Encoding.UTF8))
        {
            streamWriter.WriteLine(text);
        }
            return true;
    }
    static async Task<bool> BoolAsync(string p)
    {
        return await Task.Run(() => SaveFile(p));
    }
    private static void Main(string[] args)
    {
        #region thread
        //Thread thread = new Thread(new ThreadStart(DoWork));
        //thread.Start();
        //Thread thread2=new Thread(new ParameterizedThreadStart(DoWork2));
        //thread2.Start(int.MaxValue);
        //int j = 0;
        //for (int i = 0; i < int.MaxValue; i++)
        //{
        //    j++;
        //    if (j % 10000 == 0)
        //        Console.WriteLine("Main");
        //}
        #endregion
        //DoWorkAsync(100);

        //for (int i = 0; i < 100; i++)
        //{
        //        Console.WriteLine("Main");
        //}
        var res= BoolAsync("d:\\test.txt");
        Console.ReadLine();
        Console.WriteLine(res.Result);
    }
}